<?php
$bg = esc_attr($layout['crb_benefit_bg']);
$bg_url = wp_get_attachment_image_url($bg,'full');
$title   = esc_attr( $layout['crb_service_title'] );
$text    = esc_attr( $layout['crb_service_text'] );
?>
<section class="tm-service-section uk-block benefit"
style = "background: url(<?php echo $bg_url?>) no-repeat; -webkit-background-size: cover ;background-size: cover;">
	<div class="uk-container uk-container-center">
		<div class="uk-grid">
			<div class="uk-width-medium-1-2 uk-push-1-2">
				<h3 class="section-title"><?php echo $title; ?></h3>
				<p class="service-content"><?php echo $text; ?></p>
			</div>
		</div>
	</div>

</section>
