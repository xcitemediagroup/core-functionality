<?php
use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make( 'theme_options', 'Theme Options' )
         ->add_fields( array(
	         Field::make('checkbox','crb_css_dev_mode')
		         ->set_option_value('yes')

         ) );

//Container::make( 'theme_options', 'Testimonials slider' )
//         ->add_fields( array(
//		Field::make( 'complex', 'testimonial_slides' )->set_layout( 'tabbed-horizontal' )
//		     ->add_fields( array(
//			     Field::make( 'text', 'crb_slide_title' ),
//			     Field::make( 'textarea', 'crb_slide_content' ),
//			     Field::make( 'text', 'crb_author' ),
//		     ) ),
//
//         ) );
Container::make( 'theme_options', 'Header Video' )
         ->add_fields( array(
	         Field::make('text','crb_header_video')->help_text('write the video ID here'),
	         Field::make("file", "crb_mp4_video", "MP4 Video (MP4)"),
	         Field::make("file", "crb_webm_video", "Webm Video (webm)"),
//	         Field::make("file", "crb_ogg_video", "Ogg Video (ogg)")
         ) );