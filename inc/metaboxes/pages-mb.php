<?php
use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make( 'post_meta', 'Pages Headlines' )
         ->show_on_post_type( array( 'page' ) )
         ->add_fields( array(
	         Field::make( 'text', 'crb_headline' ),
	         Field::make( 'text', 'crb_subheadline' ),

         ) );
Container::make( 'post_meta', 'Testimonials slider' )
		->show_on_post_type( array( 'page','services' ) )
         ->add_fields( array(
	         Field::make( 'complex', 'testimonial_slides' )->set_layout( 'tabbed-horizontal' )
	              ->add_fields( array(
		              Field::make( 'text', 'crb_slide_title' ),
		              Field::make( 'textarea', 'crb_slide_content' ),
		              Field::make( 'text', 'crb_author' ),
	              ) ),

         ) );

Container::make( 'post_meta', 'Text Slider' )
         ->show_on_post_type( array( 'page' ) )
         ->show_on_page( (int) get_option( 'page_on_front' ) )
         ->add_fields( array(
	         Field::make( 'complex', 'text_slides' )->set_layout( 'tabbed-horizontal' )
	              ->add_fields( array(
		              Field::make( 'text', 'crb_slide_title' ),
		              Field::make( 'textarea', 'crb_slide_content' ),
	              ) ),
         ) );

Container::make( 'post_meta', 'Our Culture' )
         ->show_on_post_type( array( 'page' ) )
         ->show_on_page( (int) get_option( 'page_on_front' ) )
         ->add_fields( array(
	         Field::make( 'complex', 'culture_items' )->set_layout( 'tabbed-horizontal' )
	              ->add_fields( array(
		              Field::make( 'text', 'crb_culture_number' ),
		              Field::make( 'textarea', 'crb_culture_text' )->set_rows( 4 ),
	              ) ),
         ) );


Container::make( 'post_meta', 'Services Slider' )
         ->show_on_post_type( array( 'page' ) )
         ->add_fields( array(
	         Field::make( 'text', 'crb_slider_height' ),
	         Field::make( 'complex', 'services_slides' )->set_layout( 'tabbed-horizontal' )
	              ->add_fields( array(
		              Field::make( 'image', 'crb_image' ),
		              Field::make( 'text', 'crb_slider_headline' ),
		              Field::make( 'textarea', 'crb_content' )->set_rows( 5 ),
		              Field::make( 'text', 'crb_link' ),
	              ) ),
         ) );

Container::make( 'post_meta', 'Clients' )
         ->show_on_post_type( array( 'page' ) )
         ->add_fields( array(
	         Field::make( 'complex', 'clients_logos' )->set_layout( 'tabbed-horizontal' )
	              ->add_fields( array(
		              Field::make( 'image', 'crb_image' ),
		              Field::make( 'text', 'crb_link' ),
	              ) ),
         ) );
         
Container::make('post_meta', 'Subtitle')
         ->show_on_post_type('works')
         ->add_fields(array(
	         Field::make('text', 'crb_subtitle'),
         ));

Container::make('post_meta', 'More Link')
         ->show_on_post_type('entertainments')
         ->add_fields(array(
	         Field::make('text', 'crb_more_link'),
         ));


Container::make( 'post_meta', 'Team member' )
    ->show_on_post_type( 'page' )
    ->add_fields( array(
        Field::make( 'complex', 'crb_list' )->set_layout( 'tabbed-vertical' )
            ->add_fields( array(
                Field::make( 'complex', 'crb_list_item' )->set_layout( 'tabbed-horizontal' )
                    ->add_fields( array(
                        Field::make( 'image', 'team_image_normal' ),
                        Field::make( 'image', 'team_image_fun' ),
                        Field::make( 'image', 'team_image_left' ),
                        Field::make( 'image', 'team_image_right' ),
                        Field::make( 'text', 'team_index' ),
                        Field::make( 'text', 'team_name' ),
                        Field::make( 'textarea', 'team_description' )->set_rows( 4 ),
                    ) )
        ) )
    ) );

Container::make('post_meta', 'Boxes')
         ->show_on_post_type(array('entertainments','services'))
         ->add_fields(array(
	         Field::make( 'textarea', 'crb_intro_text' ),
	         Field::make( 'textarea', 'crb_left_text' ),
	         Field::make('complex', 'crb_boxes')->set_layout('tabbed-vertical')
	         ->add_fields(array(
		         Field::make( 'text', 'crb_box_title' ),
		         Field::make( 'image', 'crb_box_image' ),
		         Field::make( 'text', 'crb_box_link' ),
		         Field::make( 'textarea', 'crb_box_text' ),
		         Field::make( 'text', 'crb_website_link' ),
		         Field::make( 'text', 'crb_video_link' ),
	         ))
         ));

