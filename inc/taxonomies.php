<?php
add_action('init', 'register_case_study_category_taxonomy');

function register_case_study_category_taxonomy(){

	$labels = array(
		'name'          => _x( ' Case Study Categories', 'taxonomy general name', CHILD_TEXT_DOMAIN ),
		'singular_name' => _x( ' Case Study Categorie', 'taxonomy singular name', CHILD_TEXT_DOMAIN ),
		'menu_name'     => _x( 'Case Study Categories', 'taxonomy general name', CHILD_TEXT_DOMAIN ),
		'all_items'     => __( 'All Case Study Categories', CHILD_TEXT_DOMAIN ),
		'add_new_item'  => __( 'Add new Case Study Categorie', CHILD_TEXT_DOMAIN ),
		'edit_item'     => __( 'Edit Case Study Categorie', CHILD_TEXT_DOMAIN ),
		'add_new_item'  => __( 'Add New Case Study Categorie', CHILD_TEXT_DOMAIN ),
		'update_item'   => __( 'Update Case Study Categorie', CHILD_TEXT_DOMAIN ),
	);
	$args   = array(
		'labels'            => $labels,
		'show_in_nav_menu'  => true,
		'hierarchical'      => true,
		'show_admin_column' => true,
	);

	register_taxonomy('cs-category','works', $args);
}
add_action('init', 'register_entertainment_category_taxonomy');

function register_entertainment_category_taxonomy(){

	$labels = array(
		'name'          => _x( ' Entertainment Categories', 'taxonomy general name', CHILD_TEXT_DOMAIN ),
		'singular_name' => _x( ' Entertainment Categorie', 'taxonomy singular name', CHILD_TEXT_DOMAIN ),
		'menu_name'     => _x( 'Entertainment Categories', 'taxonomy general name', CHILD_TEXT_DOMAIN ),
		'all_items'     => __( 'All Entertainment Categories', CHILD_TEXT_DOMAIN ),
		'add_new_item'  => __( 'Add new Entertainment Category', CHILD_TEXT_DOMAIN ),
		'edit_item'     => __( 'Edit Entertainment Category', CHILD_TEXT_DOMAIN ),
		'add_new_item'  => __( 'Add New Entertainment Category', CHILD_TEXT_DOMAIN ),
		'update_item'   => __( 'Update Entertainment Category', CHILD_TEXT_DOMAIN ),
	);
	$args   = array(
		'labels'            => $labels,
		'show_in_nav_menu'  => true,
		'hierarchical'      => true,
		'show_admin_column' => true,
	);

	register_taxonomy('e-category','entertainments', $args);
}
add_action('init', 'register_services_category_taxonomy');
function register_services_category_taxonomy(){

	$labels = array(
		'name'          => _x( ' Service Categories', 'taxonomy general name', CHILD_TEXT_DOMAIN ),
		'singular_name' => _x( ' Service Category', 'taxonomy singular name', CHILD_TEXT_DOMAIN ),
		'menu_name'     => _x( 'Service Categories', 'taxonomy general name', CHILD_TEXT_DOMAIN ),
		'all_items'     => __( 'All Service Categories', CHILD_TEXT_DOMAIN ),
		'add_new_item'  => __( 'Add new Service Category', CHILD_TEXT_DOMAIN ),
		'edit_item'     => __( 'Edit Service Category', CHILD_TEXT_DOMAIN ),
		'add_new_item'  => __( 'Add New Service Category', CHILD_TEXT_DOMAIN ),
		'update_item'   => __( 'Update Service Category', CHILD_TEXT_DOMAIN ),
	);
	$args   = array(
		'labels'            => $labels,
		'show_in_nav_menu'  => true,
		'hierarchical'      => true,
		'show_admin_column' => true,
	);

	register_taxonomy('s-category','services', $args);
}