<?php
//Flush rewrite rules for custom post types
add_action( 'after_switch_theme', 'wst_flush_rewrite_rules' );

// Flush your rewrite rules
function wst_flush_rewrite_rules() {
	flush_rewrite_rules();
}

add_action( 'init', 'register_works_post_type' );
function register_works_post_type() {

	$labels = array(
		'name'          => _x( 'Case Studies', 'post type general name', CHILD_TEXT_DOMAIN ),
		'singular_name' => _x( 'Case Study', 'post type singular name', CHILD_TEXT_DOMAIN ),
		'menu_name'     => _x( 'Case Studies', 'admin menu name', CHILD_TEXT_DOMAIN ),
		'add_new'       => _x( 'Add New Case Study', 'faq', CHILD_TEXT_DOMAIN ),
		'add_new_item'  => _x( 'Add New Case Study', CHILD_TEXT_DOMAIN ),
		'search_items'  => _x( 'Search Case Study', CHILD_TEXT_DOMAIN ),
		'not_found'     => _x( 'No Case Study Found', CHILD_TEXT_DOMAIN ),

	);
	$args   = array(
		'label'        => __( 'Case Studies', CHILD_TEXT_DOMAIN ),
		'labels'       => $labels,
		'supports'     => get_cpt_supports(),
		'public'       => true,
		'taxonomies'   => array( 'category' ),
		'hierarchical' => true,
		'has_archive'  => true,
		'rewrite'      => array( 'slug' => 'case-studies' ),


	);

	register_post_type( 'works', $args );
}

add_action( 'init', 'register_entertainments_post_type' );
function register_entertainments_post_type() {

	$labels = array(
		'name'          => _x( 'Entertainment', 'post type general name', CHILD_TEXT_DOMAIN ),
		'singular_name' => _x( 'Entertainment', 'post type singular name', CHILD_TEXT_DOMAIN ),
		'menu_name'     => _x( 'Entertainment', 'admin menu name', CHILD_TEXT_DOMAIN ),
		'add_new'       => _x( 'Add New Entertainment', 'faq', CHILD_TEXT_DOMAIN ),
		'add_new_item'  => _x( 'Add New Entertainment', CHILD_TEXT_DOMAIN ),
		'search_items'  => _x( 'Search Entertainment', CHILD_TEXT_DOMAIN ),
		'not_found'     => _x( 'No Entertainment Found', CHILD_TEXT_DOMAIN ),

	);
	$args   = array(
		'label'        => __( 'Entertainment', CHILD_TEXT_DOMAIN ),
		'labels'       => $labels,
		'supports'     => get_cpt_supports(),
		'public'       => true,
		'taxonomies'   => array( 'category' ),
		'hierarchical' => true,
		'has_archive'  => false,
		'rewrite'      => array( 'slug' => 'entertainment' ),
	);

	register_post_type( 'entertainments', $args );
}

add_action( 'init', 'register_services_post_type' );
function register_services_post_type() {

	$labels = array(
		'name'          => _x( 'Services', 'post type general name', CHILD_TEXT_DOMAIN ),
		'singular_name' => _x( 'Service', 'post type singular name', CHILD_TEXT_DOMAIN ),
		'menu_name'     => _x( 'Service', 'admin menu name', CHILD_TEXT_DOMAIN ),
		'add_new'       => _x( 'Add New Service', 'faq', CHILD_TEXT_DOMAIN ),
		'add_new_item'  => _x( 'Add New Service', CHILD_TEXT_DOMAIN ),
		'search_items'  => _x( 'Search Service', CHILD_TEXT_DOMAIN ),
		'not_found'     => _x( 'No Service Found', CHILD_TEXT_DOMAIN ),

	);
	$args   = array(
		'label'        => __( 'Service', CHILD_TEXT_DOMAIN ),
		'labels'       => $labels,
		'supports'     => get_cpt_supports(),
		'public'       => true,
		'taxonomies'   => array( 'category' ),
		'hierarchical' => true,
		'has_archive'  => false,
	);

	register_post_type( 'services', $args );
}

function get_cpt_supports() {
	$all_supports = get_all_post_type_supports( 'post' );


	$all_supports = array_keys( $all_supports );

	$supports_to_exclude = array(
		'comments',
		'trackbacks',
		'post_formats',
		'custom-fields',
	);


	$supports   = array_filter( $all_supports, function ( $support ) use ( $supports_to_exclude ) {
		return ! in_array( $support, $supports_to_exclude );
	} );
	$supports[] = 'page-attributes';

	return ( $supports );


}
